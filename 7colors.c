/* Template of the 7 wonders of the world of the 7 colors assigment. */

#include <stdio.h>     /* printf */
#include <stdlib.h>
#include <time.h>

/* We want a 30x30 board game by default */
#define BOARD_SIZE 30

/** Represent the actual current board game
 *
 * NOTE: global variables are usually discouraged (plus encapsulation in
 *     an appropriate data structure would also be preferred), but don't worry.
 *     For this first assignment, no dinosaure will get you if you do that.
 */
char board[BOARD_SIZE * BOARD_SIZE] = { 0 }; // Filled with zeros

/** Retrieves the color of a given board cell */
char get_cell(int x, int y)
{
  if(0 <= x && x < BOARD_SIZE && 0 <= y && y < BOARD_SIZE)
  {
    return board[y * BOARD_SIZE + x];
  }
  else
  {
    return '0';
  }
}


/** Changes the color of a given board cell */
void set_cell(int x, int y, char color)
{
    board[y * BOARD_SIZE + x] = color;
}

void fill(char board[BOARD_SIZE * BOARD_SIZE])
{
  srand(time(NULL));
  char colors[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};

  for(int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++)
  {
    if (i != BOARD_SIZE -1 && i != (BOARD_SIZE -1) * BOARD_SIZE)
    {
      board[i] = colors[rand() % 7];
    }

    board[BOARD_SIZE -1 ] = '~';
    board[(BOARD_SIZE -1) * BOARD_SIZE] = '^';
  }
}

int is_player_in_neighbourhood(char board[BOARD_SIZE * BOARD_SIZE], int x, int y, char player)
{
  return (get_cell(x + 1, y) == player) || (get_cell(x, y + 1) == player) || (get_cell(x - 1, y ) == player) || (get_cell(x, y - 1) == player);
}

void update(char board[BOARD_SIZE * BOARD_SIZE], char player, char color_played)
{
  int changes;
  do {
    changes = 0;
    for(int x = 0; x < BOARD_SIZE; x++ )
    {
      for(int y = 0; y < BOARD_SIZE; y++)
      {
        if (get_cell(x,y) == color_played && is_player_in_neighbourhood(board,x,y,player))
        {
          set_cell(x,y,player);
          changes++;
        }
      }
    }
  } while(changes > 0);

}

char human_player(char board[BOARD_SIZE * BOARD_SIZE])
{
  // Need to ask the player to type a color
  return 0;
}

/** Prints the current state of the board on screen
 *
 * Implementation note: It would be nicer to do this with ncurse or even
 * SDL/allegro, but this is not really the purpose of this assignment.
 */
void print_board(void)
{
    int i, j;
    for (i = 0; i < BOARD_SIZE; i++) {
        for (j = 0; j < BOARD_SIZE; j++) {
            printf("%c ", get_cell(i, j));
        }
        printf("\n");
    }
}

/** Program entry point */
int main(void)
{
    printf("\n\nWelcome to the 7 wonders of the world of the 7 colors\n"
	   "*****************************************************\n\n"
	   "Current board state:\n");
     fill(board);
    print_board();

    return 0; // Everything went well
}
