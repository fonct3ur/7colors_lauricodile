/* Template of the 7 wonders of the world of the 7 colors assigment. */

#include <stdio.h>     /* printf */
#include <stdlib.h>
#include <time.h>
#include "game.h"   /* this is the file with the structures */



/*
(Excusez nous le jour de rendu était un trolldi)

    /\__/\
   /`    '\
 === 0  0 ===
   \  --  /
  /        \
 /          \
|            |
 \  ||  ||  /
  \_oo__oo_/#######o

*/


void copy(char* original, char* save, int length)
/*copies a tab of known size */
{
  for(int x = 0; x < length; x++)
  {
    for(int y = 0; y < length; y++)
    {
      save[x*length + y] = original[x*length + y];
    }
  }
}




int is_player_in_neighbourhood(game_t* curr_game, int x, int y, char player_color)
/*tests if the cell (x,y) is next to a cell of color player_color*/
{
  return (get_cell(curr_game,x + 1, y) == player_color) || (get_cell(curr_game,x, y + 1) == player_color) || (get_cell(curr_game,x - 1, y ) == player_color) || (get_cell(curr_game,x, y - 1) == player_color);
}




void update(game_t* curr_game, player_t* player)
/*updates the board curr_game with the move of player */
{
  char* color_played = malloc(sizeof(char));
  void (*strategy)(game_t*,char, char*) = player->strategy;
  strategy(curr_game,player->color,color_played);
  int changes;
  do {
    changes = 0;
    for(int x = 0; x < curr_game->board_size; x++ )
    {
      for(int y = 0; y < curr_game->board_size; y++)
      {
        if (get_cell(curr_game,x,y) == *color_played && is_player_in_neighbourhood(curr_game,x,y,player->color))
        {
          set_cell(curr_game,x,y,player->color);
          changes++;
        }
      }
    }
  } while(changes > 0);
  free(color_played);
}

int simul_update(game_t* curr_game,char player_color ,char color_played)
/*here we test how the board would evolve with the move of the player, in order to count the number
of cases the player would gain -> useful for the greedy algorithm player */
{
  char* board_save = malloc(sizeof(char)*curr_game->board_size*curr_game->board_size);
  int changes;
  int total_changes = 0;
  copy(curr_game->board, board_save, curr_game->board_size); //save the value of the board

  do {
    changes = 0;
    for(int x = 0; x < curr_game->board_size; x++ )
    {
      for(int y = 0; y < curr_game->board_size; y++)
      {
        if (get_cell(curr_game,x,y) == color_played && is_player_in_neighbourhood(curr_game,x,y,player_color))
        {
          set_cell(curr_game,x,y,player_color);
          total_changes++;
        }
      }
    }
    copy(board_save, curr_game->board, curr_game->board_size); //restore the saved value in the game
  } while(changes > 0);
  free(board_save);
  return total_changes;
}



int count(game_t* curr_game, player_t* player)
/*computes the size of the territoty owned by player */
{
  char color = player->color;
  int counter = 0;
  for( int i = 0; i < curr_game->board_size; i++)
  {
    for( int j = 0; j < curr_game->board_size; j++)
    {
      if (get_cell(curr_game, i, j) == color)
      {
        counter++;
      }
    }
  }
  return counter;
}

int not_finished(game_t* curr_game,player_t* player1, player_t* player2)
/*tests if the game is over, and prints the winner if it is */
{
  if(count(curr_game,player1) > curr_game->board_size*curr_game->board_size/2)
    {
      printf("Player 1 is the winner\n");
      return 0;
    }
  if(count(curr_game,player2) > curr_game->board_size*curr_game->board_size/2)
  {
    printf("Player 2 is the winner\n");
    return 0;
  }
  return 1;
}



void human_player(game_t* curr_game,char player_color, char* color_return)
/*asks a human player the moves they want to play  */
{
  char color_played,c;
  scanf("%c", &color_played );
  while((c = getchar()) != '\n' && c != EOF){}

  *color_return = color_played;
}

void random_player(game_t* curr_game,char player_color, char* color_return)
{
  char color_played;
  srand(time(NULL));
  char colors[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
  color_played= colors[rand() % 7];

  *color_return = color_played;
}

void better_random_player(game_t* curr_game, char player_color, char* color_return)
{
  char color_played;
  srand(time(NULL));
  char colors[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
  color_played= colors[rand() % 7];
  while (simul_update(curr_game, player_color, color_played) == 0)
    {
      color_played= colors[rand() % 7];
    }
  *color_return = color_played;
}


// int simul_update(game_t* curr_game,char player_color ,char color_played)



void greedy_player(game_t* curr_game,char player_color, char* color_return)
{
  char colors[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
  char best_color;
  int max = 0;
  int nb_of_update = 0;

  for(int i = 0; i < 7; i++)
  {
    nb_of_update = simul_update(curr_game, player_color, colors[i]);
    if (nb_of_update > max)
    {
      max = nb_of_update;
      best_color = colors[i];
    }
  }
  *color_return = best_color;
}






/** Program entry point */
int main(void)
{
    int board_size = 16 ;
    printf("\n\nWelcome to the 7 wonders of the world of the 7 colors\n"
     "*****************************************************\n\n"
     "Current board state:\n");
    player_t* player1 = player_create('^', &better_random_player);
    player_t* player2 = player_create('~', &greedy_player);

    game_t* curr_game = game_create(board_size);
    print_board(curr_game);


    while(not_finished(curr_game,player1,player2))
    {
      update(curr_game,player1);
      update(curr_game,player2);
      print_board(curr_game);
    }


    game_free(curr_game);
    player_free(player1);
    player_free(player2);

  return 0; // Everything went well
}
