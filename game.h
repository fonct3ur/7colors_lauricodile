#ifndef BOARD_H
#define BOARD_H


typedef struct {
  int board_size ;
  char* board;
} game_t ;

game_t* game_create(int board_size);

void game_free(game_t* curr_game);

char get_cell(game_t* curr_game, int x, int y);

void set_cell(game_t* curr_game, int x, int y, char color);

void print_board(game_t* curr_game);

typedef struct {
  char color;
  void (*strategy)(game_t*,char,char*);
} player_t;


player_t* player_create(char color, void* strategy);

void player_free(player_t* player);


#endif
