CC = gcc
CFLAGS = -g -o -Wall -Werror -Wextra -Wno-unused-function -Wno-unused-parameter
PROG = 7color_structured
OBJS = 7color_structured.o game.o

$(PROG) : $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

.PHONY : clean

clean:
	rm -f $(OBJS) $(PROG)
