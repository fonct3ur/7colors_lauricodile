#include "game.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>





game_t* game_create(int board_size)
/*creates a board and initializes it with random colors */
{
  char * curr_board = malloc(sizeof(int)*board_size*board_size) ;
  srand(time(NULL));
  char colors[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};

  for(int i = 0; i < board_size * board_size; i++)
  {
    if (i != board_size -1 && i != (board_size -1) * board_size)
    {
      curr_board[i] = colors[rand() % 7];
    }

    curr_board[board_size -1 ] = '~';
    curr_board[(board_size -1) * board_size] = '^';
  }
  game_t * curr_game = malloc(sizeof(game_t));
  curr_game->board_size = board_size;
  curr_game->board = curr_board;

  return curr_game;

}


void game_free(game_t* curr_game)
{
  free(curr_game->board);
  free(curr_game);
}


char get_cell(game_t* curr_game, int x, int y)
{
  if(0 <= x && x < curr_game->board_size && 0 <= y && y < curr_game->board_size)
  {
    return curr_game->board[y * curr_game->board_size + x];
  }
  else
  {
    return '0'; //out borders cases contains zero -> easier when you want to check neighbour of a border case
  }
}

void set_cell(game_t* curr_game, int x, int y, char color)
{
      curr_game->board[y * curr_game->board_size + x] = color;
}


void print_board(game_t* curr_game)
{
    int i, j;
    for (i = 0; i < curr_game->board_size; i++) {
        for (j = 0; j < curr_game->board_size; j++) {
            if( get_cell(curr_game, i, j) == '^')
            {
              printf("\033[0;31m%c\033[0m ", get_cell(curr_game, i, j));
            }
            else if(get_cell(curr_game, i, j) == '~')
            {
              printf("\033[0;36m%c\033[0m ", get_cell(curr_game, i, j));
            }
            else
            {
              printf("%c ", get_cell(curr_game, i, j));
           }
        }
        printf("\n");
    }
}



player_t* player_create(char color, void* strategy)
{
  player_t* player = malloc(sizeof(player_t));
  player->color = color;
  player->strategy = strategy;

  return player;
}

void player_free(player_t* player)
{
  free(player);
}
